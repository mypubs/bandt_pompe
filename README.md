# README #

This repository contains a set of functions to perform the Bandt-Pompe transformations in time series.

It contains functions to create the set of ordinal patterns from the Bandt-Pompe transformations, the Bandt-Pompe probability distribution, and the construction of transition graphs from the ordinal patterns after the data transformation.

Also, it contains codes for the computation of information theory quantifiers and graph measures, related to these transformations.